use itertools::Itertools;

#[aoc_generator(day9)]
fn parse_input(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|x| x.parse::<usize>().unwrap())
        .collect()
}

#[aoc(day9, part1)]
fn part1(input: &[usize]) -> usize {
    for idx in 0..input.len() - 25 {
        let num = input[idx + 25];
        let pre = input.get(idx..idx+25).unwrap();
        if !pre.iter().combinations(2).any(|nums| nums[0] + nums[1] == num) {
            return num;
        }
    }
    0
}

#[aoc(day9, part2)]
fn part2(input: &[usize]) -> usize {
    let needle = part1(input);
    for stack in 2..input.len() {
        for idx in 0..input.len() - stack {
            let nums = input.get(idx..idx+stack).unwrap();
            let sum: usize = nums.iter().sum();
            if sum == needle {
                let min = nums.iter().min().unwrap();
                let max = nums.iter().max().unwrap();
                return min + max;
            }
        }
    }
    0
}
