#[derive(Debug, Clone, Copy)]
enum Op {
    Add,
    Mul,
}

struct Calc {
    pub add_pre: bool,
    op: Option<Op>,
    num: Option<isize>,
    muls: Vec<isize>,
}

impl Calc {
    pub fn new(add_pre: bool) -> Self {
        Self { add_pre, op: None, num: None, muls: Vec::new() }
    }

    pub fn push_num(&mut self, num: isize) {
        if let Some(prev) = self.num {
            if let Some(op) = self.op {
                match op {
                    Op::Add => self.num = Some(prev + num),
                    Op::Mul => if self.add_pre {
                        self.muls.push(self.num.unwrap());
                        self.num = Some(num);
                    } else {
                        self.num = Some(prev * num);
                    },
                }
            }
        } else {
            self.num = Some(num);
        }
    }

    pub fn get_num(&self) -> isize {
        if self.add_pre {
            self.muls.iter().fold(self.num.unwrap(), |a,x| a * x)
        } else {
            self.num.unwrap()
        }
    }

    pub fn set_op(&mut self, op: Op) {
        self.op = Some(op);
    }
}

#[aoc_generator(day18)]
fn parse_input(input: &str) -> Vec<String> {
    input.lines().map(|s| s.chars().filter(|c| !c.is_whitespace()).collect::<String>()).collect()
}

fn calc_line(line: &str, mut calc: Calc) -> isize {
    let mut start = None;
    let mut level = 0;

    for (i, c) in line.chars().enumerate() {
        match c {
            '+' => if level == 0 { calc.set_op(Op::Add) },
            '*' => if level == 0 { calc.set_op(Op::Mul) },
            '(' => {
                if start.is_none() {
                    start = Some(i);
                }
                level += 1;
            }
            ')' => {
                level -= 1;
                if level == 0 {
                    calc.push_num(calc_line(&line[start.unwrap()+1..i], Calc::new(calc.add_pre)));
                    start = None;
                }
            }
            _ => if level == 0 { calc.push_num(c.to_string().parse::<isize>().unwrap()) },
        }
    }

    calc.get_num()
}

#[aoc(day18, part1)]
fn part1(input: &[String]) -> isize {
    input.iter().map(|line| calc_line(line, Calc::new(false))).sum()
}

#[aoc(day18, part2)]
fn part2(input: &[String]) -> isize {
    input.iter().map(|line| calc_line(line, Calc::new(true))).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"2 * 3 + (4 * 5)";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 26);
    }

    #[test]
    fn test_part2() {
        let inp = r"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 23340);
    }
}
