use std::collections::HashMap;

#[aoc_generator(day15)]
fn parse_input(input: &str) -> Vec<usize> {
    input.lines().next().unwrap().split(',')
        .filter_map(|x| x.parse::<usize>().ok())
        .collect()
}

fn play(input: &[usize], turn: usize) -> usize {
    let mut map = HashMap::new();
    input.iter().enumerate().for_each(|(idx, val)| {map.insert(*val, idx + 1);});
    (input.len()+1..turn)
        .fold(0, |acc, idx| {
            match map.insert(acc, idx) {
                Some(last) => idx - last,
                None => 0,
            }
        })
}

#[aoc(day15, part1)]
fn part1(input: &[usize]) -> usize {
    play(input, 2020)
}

#[aoc(day15, part2)]
fn part2(input: &[usize]) -> usize {
    play(input, 30000000)
}
