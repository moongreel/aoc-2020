use std::collections::HashMap;
use regex::Regex;

#[derive(Clone, Debug)]
enum Instruction {
    MaskChange(Mask),
    Memset(Mem),
}

#[derive(Clone, Debug)]
struct Mask {
    ones: u64,
    zeroes: u64,
    floating: Vec<usize>,
}

#[derive(Copy, Clone, Debug)]
struct Mem {
    pub idx: u64,
    pub val: u64,
}

#[derive(Debug)]
struct MaskIter<'a> {
    addr: u64,
    val: u64,
    max: u64,
    floating: &'a [usize],
}

impl<'a> MaskIter<'a> {
    pub fn new(addr: u64, floating: &'a [usize]) -> Self {
        Self {
            addr,
            val: 0,
            max: 1 << floating.len(),
            floating,
        }
    }
}

impl<'a> Iterator for MaskIter<'a> {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if self.val < self.max {
            let mut addr = self.addr;
            self.floating.iter().enumerate().for_each(|(idx, bit)| {
                let bitval = (self.val >> idx) & 1;
                if bitval == 1 {
                    addr |= 1 << bit;
                } else {
                    let bits = 0xFFFF_FFFF_FFFF_FFFF ^ (1 << bit);
                    addr &= bits;
                }
            });
            self.val += 1;
            Some(addr)
        } else {
            None
        }
    }
}

impl Mask {
    pub fn apply(&self, num: u64) -> u64 {
        (num & self.zeroes) | self.ones
    }

    pub fn iter(&self, addr: u64) -> MaskIter {
        MaskIter::new(addr | self.ones, &self.floating)
    }
}

#[aoc_generator(day14)]
fn parse_input(input: &str) -> Vec<Instruction> {
    let mempat = Regex::new(r"^mem\[(\d+)\] = (\d+)$").unwrap();
    let maskpat = Regex::new(r"^mask = ([X01]+)$").unwrap();

    input
        .lines()
        .filter_map(|line| {
            if maskpat.is_match(line) {
                let caps = maskpat.captures(line).unwrap();
                let mask = caps.get(1).unwrap().as_str();
                let mut zeroes: u64 = 0;
                let mut ones: u64 = 0xFFFF_FFFF_FFFF_FFFF;
                let mut floating = Vec::new();

                mask.chars().rev().enumerate().for_each(|(idx, c)| {
                    match c {
                        '0' => zeroes ^= 1 << idx,
                        '1' => ones ^= 1 << idx,
                        'X' => floating.push(idx),
                        _ => (),
                    }
                });

                let ones = !ones;
                let zeroes = !zeroes;

                Some(Instruction::MaskChange(Mask { ones, zeroes, floating }))
            } else if mempat.is_match(line) {
                let caps = mempat.captures(line).unwrap();
                let idx = caps.get(1).unwrap().as_str().parse::<u64>().unwrap();
                let val = caps.get(2).unwrap().as_str().parse::<u64>().unwrap();

                Some(Instruction::Memset(Mem { idx, val }))
            } else {
                None
            }
        })
        .collect()
}

#[aoc(day14, part1)]
fn part1(input: &[Instruction]) -> u64 {
    let mut memory = HashMap::new();
    let mut mask = Mask { zeroes: 0, ones: 0, floating: Vec::new() };

    input.iter().for_each(|ins| {
        match ins {
            Instruction::MaskChange(new_mask) => mask = new_mask.clone(),
            Instruction::Memset(mem) => { 
                memory.insert(mem.idx, mask.apply(mem.val));
            }
        }
    });

    memory.iter().map(|(_, val)| *val).sum()
}

#[aoc(day14, part2)]
fn part2(input: &[Instruction]) -> u64 {
    let mut memory = HashMap::new();
    let mut mask = Mask { zeroes: 0, ones: 0, floating: Vec::new() };

    input.iter().for_each(|ins| {
        match ins {
            Instruction::MaskChange(new_mask) => {
                mask = new_mask.clone();
            }
            Instruction::Memset(mem) => { 
                mask.iter(mem.idx).for_each(|addr| {
                    memory.insert(addr, mem.val);
                })
            }
        }
    });

    memory.iter().map(|(_, val)| *val).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 165);
    }

    #[test]
    fn test_part2() {
        let inp = r"mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 208);
    }
}
