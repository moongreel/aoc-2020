#[aoc_generator(day2)]
fn parse_input(input: &str) -> Vec<(usize, usize, char, String)> {
    input
        .lines()
        .map(|x| {
            let x: Vec<_> = x.split(':').collect();
            let passwd = x[1].replace(" ", "");
            let x: Vec<_> = x[0].split(' ').collect();
            let c = x[1].chars().next().unwrap();
            let x: Vec<_> = x[0].split('-').collect();
            let min = x[0].parse::<usize>().unwrap();
            let max = x[1].parse::<usize>().unwrap();

            (min, max, c, passwd)
        })
        .collect()
}

#[aoc(day2, part1)]
fn part1(lines: &[(usize, usize, char, String)]) -> u64 {
    let mut res = 0;

    for (min, max, c, passwd) in lines {
        let num = passwd
            .chars()
            .filter(|x| x == c)
            .count();

        if num >= *min && num <= *max {
            res += 1;
        }
    }

    res
}

#[aoc(day2, part2)]
fn part2(lines: &[(usize, usize, char, String)]) -> u64 {
    let mut res = 0;

    for (min, max, c, passwd) in lines {

        let a = passwd.chars().nth(*min - 1).unwrap() == *c;
        let b = passwd.chars().nth(*max - 1).unwrap() == *c;

        if a ^ b {
            res += 1;
        }
    }

    res
}
