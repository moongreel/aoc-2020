use regex::Regex;

#[aoc_generator(day13)]
fn parse_input(input: &str) -> (isize, Vec<Option<isize>>) {
    let pat = Regex::new(r"\d+").unwrap();
    let mut lines = input.lines();
    let ts = lines.next().unwrap().parse::<isize>().unwrap();
    let busses = lines.next().unwrap()
        .split(',')
        .map(|x| {
            if pat.is_match(x) {
                Some(x.parse::<isize>().unwrap())
            } else { None }
        })
        .collect();

    (ts, busses)
}

#[aoc(day13, part1)]
fn part1(input: &(isize, Vec<Option<isize>>)) -> isize {
    let ts = input.0;
    let busses = input.1.clone();

    let (id, minutes) = busses
        .iter()
        .filter_map(|x| *x)
        .map(|x| (x, x - (ts % x)))
        .min_by_key(|x| x.1)
        .unwrap();

    id * minutes
}

fn extended_euclid(a: isize, b: isize) -> isize {
    let (mut r0, mut r1) = (a, b);
    let (mut s0, mut s1) = (1, 0);

    loop {
        if r1 == 0 {
            break;
        }

        let q = r0 / r1;

        let tmp_r = r1;
        r1 = r0 - q * r1;
        r0 = tmp_r;

        let tmp_s = s1;
        s1 = s0 - q * s1;
        s0 = tmp_s;
    }

    s0
}

#[allow(non_snake_case)]
#[aoc(day13, part2)]
fn part2(input: &(isize, Vec<Option<isize>>)) -> isize {
    let timing: Vec<(isize, isize)> = input.1
        .iter()
        .enumerate()
        .filter_map(|(idx, x)| if x.is_some() { Some((idx as isize, x.unwrap())) } else { None })
        .collect();

    let mut s = 0;
    let N = timing.iter().fold(1, |acc, (_, x)| acc*x);
    for (a, n) in timing.iter() {
        let a = if *a == 0 { 0 } else { n - a };
        let Ni = N/n;
        let M = extended_euclid(Ni, *n);
        s += a*M*Ni;
    }

    if s < 0 {
        N - (s % N).abs()
    } else {
        s % N
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"939
7,13,x,x,59,x,31,19";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 5 * 59);
    }

    #[test]
    fn test_part2_1() {
        let inp = r"939
17,x,13,19";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 3417);
    }
    #[test]
    fn test_part2_2() {
        let inp = r"939
7,13,x,x,59,x,31,19";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 1068781);
    }
}
