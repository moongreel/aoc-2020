use std::collections::HashSet;
use std::ops::Range;
use regex::Regex;

#[derive(Debug, Clone)]
struct Rule {
    pub name: String,
    pub ranges: Vec<Range<isize>>,
}

impl Rule {
    pub fn contains(&self, num: &isize) -> bool {
        self.ranges.iter().any(|x| x.contains(num))
    }
}

#[aoc_generator(day16)]
fn parse_input(input: &str) -> (Vec<Rule>, Vec<isize>, Vec<Vec<isize>>) {
    let rangepat = Regex::new(r"(\d+)-(\d+)").unwrap();
    let mut sections = input.split("\n\n");

    let rules: Vec<Rule> = sections.next().unwrap()
        .lines()
        .map(|rule| {
            let mut rule = rule.split(':');
            let name = rule.next().unwrap().to_string();
            let ranges = rule.next().unwrap().split(" or ")
                .map(|range| {
                    let cap = rangepat.captures(range).unwrap();
                    let start = cap.get(1).unwrap().as_str().parse::<isize>().unwrap();
                    let end = cap.get(2).unwrap().as_str().parse::<isize>().unwrap();

                    start..end+1
                })
                .collect();
            Rule { name, ranges }
        }).collect();

    let myticket: Vec<isize> = sections.next().unwrap()
        .lines()
        .nth(1)
        .unwrap()
        .split(',')
        .filter_map(|x| x.parse::<isize>().ok())
        .collect();

    let tickets: Vec<Vec<isize>> = sections.next().unwrap()
        .lines()
        .map(|line| line.split(',').filter_map(|x| x.parse::<isize>().ok()).collect::<Vec<isize>>())
        .filter(|x| !x.is_empty())
        .collect();

    (rules, myticket, tickets)
}

#[aoc(day16, part1)]
fn part1(input: &(Vec<Rule>, Vec<isize>, Vec<Vec<isize>>)) -> isize {
    let rules = input.0.clone();
    let tickets = input.2.clone();

    tickets
        .iter()
        .map(|ticket| ticket.iter().filter(|num|
                !rules.iter().any(|rule| rule.contains(num))
            ).sum::<isize>()
        ).sum()
}

#[aoc(day16, part2)]
fn part2(input: &(Vec<Rule>, Vec<isize>, Vec<Vec<isize>>)) -> isize {
    let rules = input.0.clone();
    let myticket = input.1.clone();
    let tickets = input.2.clone();

    let valid: Vec<Vec<isize>> = tickets.into_iter()
        .filter(|ticket| ticket.iter().all(|x| rules.iter().any(|rule| rule.contains(x))))
        .collect();

    let sets: Vec<Vec<HashSet<&str>>> = valid.iter()
        .map(|ticket|
            ticket.iter().map(|num|
                rules.iter().filter_map(|rule|
                    if rule.contains(num) { Some(rule.name.as_ref()) }
                    else { None }
                ).collect()
            ).collect()
        ).collect();

    let num_fields = myticket.len();
    let mut fields = Vec::new();

    for idx in 0..num_fields {
        let golden: HashSet<&str> = rules.iter().map(|x| x.name.as_ref()).collect();
        let field = sets.iter().fold(golden, |acc, set| acc.intersection(&set[idx]).copied().collect());
        fields.push(field);
    }

    loop {
        if fields.iter().all(|x| x.len() == 1) {
            break;
        }

        let mut tmp = fields.clone();
        for (i, field) in fields.iter().enumerate() {
            if field.len() == 1 {
                let s = field.iter().next().unwrap();
                for (j, tfield) in tmp.iter_mut().enumerate() {
                    if j != i {
                        tfield.remove(s);
                    }
                }
            }
        }
        fields = tmp;
    }

    let fields: Vec<&str> = fields.into_iter().map(|x| x.into_iter().next().unwrap()).collect();
    let pat = "departure";

    fields.iter().enumerate().fold(1, |acc, (i, field)| {
        if field.len() >= pat.len() && &field[..pat.len()] == pat {
            acc * myticket[i]
        } else { acc }
    })
}
