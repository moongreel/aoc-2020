use std::collections::HashSet;

struct Group {
    people: Vec<HashSet<char>>,
}

impl Group {
    pub fn new() -> Self {
        Self {
            people: Vec::new(),
        }
    }

    pub fn add_person(&mut self, p: HashSet<char>) {
        self.people.push(p);
    }

    pub fn all_answers(&self) -> usize {
        self.people
            .iter()
            .fold(HashSet::new(), |res, p| res.union(&p).cloned().collect())
            .len()
    }

    pub fn same_answers(&self) -> usize {
        let init: HashSet<char> = "abcdefghijklmnopqrstuvwxyz".chars().collect();
        self.people
            .iter()
            .fold(init, |res, p| res.intersection(&p).cloned().collect())
            .len()
    }
}

#[aoc_generator(day6)]
fn parse_input(input: &str) -> Vec<Group> {
    let mut groups = Vec::new();
    let mut group = Group::new();

    for line in input.lines() {
        if line.is_empty() {
            groups.push(group);
            group = Group::new();
            continue;
        }

        let person = line.chars().collect();
        group.add_person(person);
    }

    groups.push(group);

    groups
}

#[aoc(day6, part1)]
fn part1(input: &[Group]) -> usize {
    input
        .iter()
        .map(|group| group.all_answers())
        .sum::<usize>()
}

#[aoc(day6, part2)]
fn part2(input: &[Group]) -> usize {
    input
        .iter()
        .map(|group| group.same_answers())
        .sum::<usize>()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"abc

a
b
c

ab
ac

a
a
a
a

b";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 11);
    }
    
    #[test]
    fn test_part2() {
        let inp = r"abc

a
b
c

ab
ac

a
a
a
a

b";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 6);
    }
}
