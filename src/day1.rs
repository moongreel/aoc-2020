#[aoc_generator(day1)]
fn parse_input(input: &str) -> Vec<u64> {
    input
        .lines()
        .filter_map(|x| x.parse::<u64>().ok())
        .collect()
}

#[aoc(day1, part1)]
fn part1(lines: &[u64]) -> u64 {
    for (i, n1) in lines.iter().enumerate() {
        for n2 in lines[i..].iter() {
            if n1 + n2 == 2020 {
                return n1 * n2;
            }
        }
    }

    panic!()
}

#[aoc(day1, part2)]
fn part2(lines: &[u64]) -> u64 {
    for (i, n1) in lines.iter().enumerate() {
        for (j, n2) in lines[i..].iter().enumerate() {
            for n3 in lines[j..].iter() {
                if n1 + n2 + n3 == 2020 {
                    return n1 * n2 * n3;
                }
            }
        }
    }

    panic!()
}
