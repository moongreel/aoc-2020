use std::collections::HashSet;

#[aoc_generator(day5)]
fn parse_input(input: &str) -> Vec<Vec<usize>> {
    input
        .lines()
        .map(|x| x.chars().map(|x| {
                match x {
                    'F' => 0,
                    'B' => 1,
                    'R' => 2,
                    'L' => 3,
                    _ => panic!("Bad char: {}", x),
                }
            }).collect()
        )
        .collect()
}

#[aoc(day5, part1)]
fn part1(input: &[Vec<usize>]) -> usize {
    let mut max_id = 0;

    for seat in input.iter() {
        let mut row = 0;
        let mut col = 0;

        let mut rstart = 0;
        let mut rend = 127;
        let mut rdelta = 64;
        let mut cstart = 0;
        let mut cend = 7;
        let mut cdelta = 4;

        for x in seat.iter() {
            match x {
                0 => {
                    rend -= rdelta;
                    rdelta /= 2;
                    row = rstart;
                },
                1 => {
                    rstart += rdelta;
                    rdelta /= 2;
                    row = rend;
                },
                2 => {
                    cstart += cdelta;
                    cdelta /= 2;
                    col = cend;
                },
                3 => {
                    cend -= cdelta;
                    cdelta /= 2;
                    col = cstart;
                },
                _ => panic!("Bad enum"),
            }
        }
        let id = row * 8 + col;
        if id > max_id {
            max_id = id;
        }
    }
    
    max_id
}

#[aoc(day5, part2)]
fn part2(input: &[Vec<usize>]) -> usize {
    let mut seats = HashSet::new();
    for seat in input.iter() {
        let mut row = 0;
        let mut col = 0;

        let mut rstart = 0;
        let mut rend = 127;
        let mut rdelta = 64;
        let mut cstart = 0;
        let mut cend = 7;
        let mut cdelta = 4;

        for x in seat.iter() {
            match x {
                0 => {
                    rend -= rdelta;
                    rdelta /= 2;
                    row = rstart;
                },
                1 => {
                    rstart += rdelta;
                    rdelta /= 2;
                    row = rend;
                },
                2 => {
                    cstart += cdelta;
                    cdelta /= 2;
                    col = cend;
                },
                3 => {
                    cend -= cdelta;
                    cdelta /= 2;
                    col = cstart;
                },
                _ => panic!("Bad enum"),
            }
        }
        let id = row * 8 + col;
        seats.insert(id);
    }

    for row in 1..127 {
        for col in 0..8 {
            let id = row * 8 + col;
            if !seats.contains(&id) && seats.contains(&(id - 1)) && seats.contains(&(id + 1)) {
                return id;
            }
        }
    }

    panic!();
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"FBFBBFFRLR";
        let inp = parse_input(inp);

        assert_eq!(part1(&inp), 357);
    }
}
