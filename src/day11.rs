use std::cmp::{max,min};

#[derive(Debug, Clone, PartialEq, Copy)]
enum Tile {
    Floor,
    Empty,
    Occupied,
}

#[aoc_generator(day11)]
fn parse_input(input: &str) -> (Vec<Tile>, usize) {
    (input.chars()
     .filter_map(|c| match c {
         '.' => Some(Tile::Floor),
         'L' => Some(Tile::Empty),
         '#' => Some(Tile::Occupied),
         _ => None,
     }).collect(),
     input.lines().last().unwrap().len())
}

fn num_occupied(x: isize, y: isize, width: usize, grid: &[Tile]) -> usize {
    (max(0, y-1)..min((grid.len()/width) as isize, y+2))
        .map(|j| (max(0, x-1)..min(width as isize, x+2))
            .filter(|i| grid.get((j as usize)*width + *i as usize) == Some(&Tile::Occupied))
            .count()
        ).sum()
}

#[aoc(day11, part1)]
fn part1(input: &(Vec<Tile>, usize)) -> usize {
    let mut grid = input.0.clone();
    let mut tmp = grid.clone();
    let width = input.1;

    loop {
        let mut changed = false;
        for (idx, seat) in grid.iter().enumerate() {
            let x = idx % width;
            let y = idx / width;

            match seat {
                Tile::Empty => {
                    if num_occupied(x as isize, y as isize, width, &grid) == 0 {
                        tmp[idx] = Tile::Occupied;
                        changed = true;
                    }
                },
                Tile::Occupied => {
                    if num_occupied(x as isize, y as isize, width, &grid) > 4 {
                        tmp[idx] = Tile::Empty;
                        changed = true;
                    }
                },
                _ => {},
            }
        }

        if !changed {
            break;
        }

        grid.clear();
        grid.extend_from_slice(&tmp);
    }

    grid.iter().filter(|x| **x == Tile::Occupied).count()
}

struct Ray {
    pub x: isize,
    pub y: isize,
    pub dx: isize,
    pub dy: isize,
    pub terminated: bool,
    pub occupied: bool,
}

impl Ray {
    pub fn new(x: isize, y: isize, dx: isize, dy: isize) -> Self {
        Self {
            terminated: false,
            x, y, dx, dy,
            occupied: false,
        }
    }

    pub fn step(&mut self, width: usize, seats: &[Tile]) {
        if self.terminated {
            return;
        }

        let height = seats.len() / width;

        self.x += self.dx;
        self.y += self.dy;

        if self.x >= width as isize || self.x < 0 {
            self.terminated = true;
            return;
        }

        if self.y >= height as isize || self.y < 0 {
            self.terminated = true;
            return;
        }

        let x = self.x as usize;
        let y = self.y as usize;

        match seats[y*width + x] {
            Tile::Occupied => {
                self.terminated = true;
                self.occupied = true;
            },
            Tile::Empty => {
                self.terminated = true;
            }
            _ => {}
        }
    }
}

fn seat_check_ray_cast(x: usize, y: usize, width: usize, seats: &[Tile]) -> usize {
    let x = x as isize;
    let y = y as isize;

    let mut rays: Vec<Ray> = vec![
        Ray::new(x, y, 1, 0),
        Ray::new(x, y, 0, 1),
        Ray::new(x, y, -1, 0),
        Ray::new(x, y, 0, -1),
        Ray::new(x, y, 1, 1),
        Ray::new(x, y, 1, -1),
        Ray::new(x, y, -1, 1),
        Ray::new(x, y, -1, -1),
    ];

    loop {
        if rays.iter().all(|r| r.terminated) {
            break;
        }

        rays.iter_mut().for_each(|r| r.step(width, seats));
    }

    rays.iter().filter(|r| r.occupied).count()
}

#[aoc(day11, part2)]
fn part2(input: &(Vec<Tile>, usize)) -> usize {
    let mut grid = input.0.clone();
    let mut tmp = grid.clone();
    let width = input.1;

    loop {
        let mut changed = false;
        for (idx, seat) in grid.iter().enumerate() {
            let x = idx % width;
            let y = idx / width;

            match seat {
                Tile::Empty => {
                    if seat_check_ray_cast(x, y, width, &grid) == 0 {
                        tmp[idx] = Tile::Occupied;
                        changed = true;
                    }
                },
                Tile::Occupied => {
                    if seat_check_ray_cast(x, y, width, &grid) >= 5 {
                        tmp[idx] = Tile::Empty;
                        changed = true;
                    }
                },
                _ => {},
            }
        }

        if !changed {
            break;
        }

        grid.clear();
        grid.extend_from_slice(&tmp);
    }

    grid.iter().filter(|x| **x == Tile::Occupied).count()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 37);
    }

    #[test]
    fn test_part2() {
        let inp = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 26);
    }
}
