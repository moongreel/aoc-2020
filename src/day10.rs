use std::collections::{
    HashMap,
};

#[derive(Debug, Clone)]
struct Node {
    pub into: usize,
    pub out: usize,
}

#[aoc_generator(day10)]
fn parse_input(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|x| x.parse::<usize>().unwrap())
        .collect()
}

#[aoc(day10, part1)]
fn part1(input: &[usize]) -> usize {
    let mut input = input.to_vec();
    input.sort_unstable();
    let mut joltage = 0;
    let mut ones = 0;
    let mut threes = 0;
    for num in input {
        if num - joltage == 1 {
            ones += 1;
        } else if num - joltage == 3 {
            threes += 1;
        }
        joltage = num;
    }

    ones * (threes+1)
}

#[aoc(day10, part2)]
fn part2(input: &[usize]) -> usize {
    let mut map: HashMap<usize, usize> = HashMap::new();
    let mut input = input.to_vec();
    input.sort_unstable();
    let max = input.iter().max().unwrap();

    map.insert(0, 1);
    input
        .iter()
        .for_each(|x| {
            let out: usize = map
                .iter()
                .filter(|(k, _)| *k + 3 >= *x && *k < x).map(|(_, v)| *v)
                .sum();

            map.insert(*x, out);
        });

    *map.get(max).unwrap()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1_1() {
        let inp = r"16
10
15
5
1
11
7
19
6
12
4";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 7 * 5);
    }

    #[test]
    fn test_part1_2() {
        let inp = r"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 22 * 10);
    }

    #[test]
    fn test_part2_1() {
        let inp = r"16
10
15
5
1
11
7
19
6
12
4";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 8);
    }

    #[test]
    fn test_part2_2() {
        let inp = r"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 19208);
    }
}
