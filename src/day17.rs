use std::collections::HashSet;

#[aoc_generator(day17)]
fn parse_input(input: &str) -> Vec<Vec<bool>> {
    input
        .lines()
        .map(|line| line.chars().map(|c| match c {
            '#' => true,
            '.' => false,
            _ => panic!(),
        }).collect())
        .collect()
}

fn get_active_neighbors(x: isize, y: isize, z: isize, set: &HashSet<(isize, isize, isize)>) -> usize{
    (z-1..z+2).map(|zc| {
        (y-1..y+2).map(|yc| {
            (x-1..x+2).filter(|xc| {
                if x == *xc && y == yc && z == zc { false }
                else { set.contains(&(*xc, yc, zc)) }
            }).count()
        }).sum::<usize>()
    }).sum()
}

#[aoc(day17, part1)]
fn part1(input: &[Vec<bool>]) -> usize {
    let mut set: HashSet<(isize, isize, isize)> = HashSet::new();

    input.iter().enumerate().for_each(|(y, row)| {
        row.iter().copied().enumerate().for_each(|(x, state)| {
            if state {
                set.insert((x as isize, y as isize, 0));
            }
        });
    });

    for _cycle in 0..6 {
        let mut tmp = set.clone();
        let bounds = (
            (set.iter().map(|coord| coord.0).min().unwrap(),
             set.iter().map(|coord| coord.0).max().unwrap()),
            (set.iter().map(|coord| coord.1).min().unwrap(),
             set.iter().map(|coord| coord.1).max().unwrap()),
            (set.iter().map(|coord| coord.2).min().unwrap(),
             set.iter().map(|coord| coord.2).max().unwrap()),
        );
        (bounds.2.0-1..bounds.2.1+2).for_each(|z| {
            (bounds.1.0-1..bounds.1.1+2).for_each(|y| {
                (bounds.0.0-1..bounds.0.1+2).for_each(|x| {
                    let n = get_active_neighbors(x, y, z, &set);
                    if set.contains(&(x, y, z)) && (n < 2 || n > 3)  {
                        tmp.remove(&(x, y, z));
                    } else if !set.contains(&(x, y, z)) && n == 3 {
                        tmp.insert((x, y, z));
                    }
                });
            });
        });
        set = tmp;
    }

    set.iter().count()
}

fn get_hyper_neighbors(x: isize, y: isize, z: isize, w: isize, set: &HashSet<(isize, isize, isize, isize)>) -> usize{
    (w-1..w+2).map(|wc| {
        (z-1..z+2).map(|zc| {
            (y-1..y+2).map(|yc| {
                (x-1..x+2).filter(|xc| {
                    if x == *xc && y == yc && z == zc && w == wc { false }
                    else { set.contains(&(*xc, yc, zc, wc)) }
                }).count()
            }).sum::<usize>()
        }).sum::<usize>()
    }).sum()
}

#[aoc(day17, part2)]
fn part2(input: &[Vec<bool>]) -> usize {
    let mut set: HashSet<(isize, isize, isize, isize)> = HashSet::new();

    input.iter().enumerate().for_each(|(y, row)| {
        row.iter().copied().enumerate().for_each(|(x, state)| {
            if state {
                set.insert((x as isize, y as isize, 0, 0));
            }
        });
    });

    for _cycle in 0..6 {
        let mut tmp = set.clone();
        let bounds = (
            (set.iter().map(|coord| coord.0).min().unwrap(),
             set.iter().map(|coord| coord.0).max().unwrap()),
            (set.iter().map(|coord| coord.1).min().unwrap(),
             set.iter().map(|coord| coord.1).max().unwrap()),
            (set.iter().map(|coord| coord.2).min().unwrap(),
             set.iter().map(|coord| coord.2).max().unwrap()),
            (set.iter().map(|coord| coord.3).min().unwrap(),
             set.iter().map(|coord| coord.3).max().unwrap()),
        );

        (bounds.3.0-1..bounds.3.1+2).for_each(|w| {
            (bounds.2.0-1..bounds.2.1+2).for_each(|z| {
                (bounds.1.0-1..bounds.1.1+2).for_each(|y| {
                    (bounds.0.0-1..bounds.0.1+2).for_each(|x| {
                        let n = get_hyper_neighbors(x, y, z, w, &set);
                        if set.contains(&(x, y, z, w)) && (n < 2 || n > 3)  {
                            tmp.remove(&(x, y, z, w));
                        } else if !set.contains(&(x, y, z, w)) && n == 3 {
                            tmp.insert((x, y, z, w));
                        }
                    });
                });
            });
        });
        set = tmp;
    }

    set.iter().count()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let inp = r".#.
..#
###";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 112);
    }
}
