#[derive(Debug, Clone)]
struct Instruction {
    pub amt: isize,
    pub dir: Dir,
}

#[derive(Debug, Clone)]
enum Dir {
    North,
    South,
    East,
    West,
    Left,
    Right,
    Forward,
}

#[aoc_generator(day12)]
fn parse_input(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|x| {
            let dir = x.get(0..1);
            let amt = x.get(1..).unwrap().parse::<isize>().unwrap();

            let dir = match dir.unwrap() {
                "N" => Dir::North,
                "S" => Dir::South,
                "E" => Dir::East,
                "W" => Dir::West,
                "L" => Dir::Left,
                "R" => Dir::Right,
                "F" => Dir::Forward,
                _ => panic!(),
            };

            Instruction { amt, dir }
        })
        .collect()
}

#[aoc(day12, part1)]
fn part1(input: &[Instruction]) -> isize {
    let mut position = (0, 0);
    let cardinal = [Dir::East, Dir::South, Dir::West, Dir::North];
    let mut cidx: isize = 0;

    for ins in input {
        let dir = if let Dir::Forward = ins.dir {
            &cardinal[cidx as usize]
        } else {
            &ins.dir
        };

        match dir {
            Dir::North => position.1 += ins.amt,
            Dir::South => position.1 -= ins.amt,
            Dir::East => position.0 += ins.amt,
            Dir::West => position.0 -= ins.amt,

            Dir::Right => {
                let amt = ins.amt / 90;
                cidx += amt;
                cidx %= cardinal.len() as isize;
            },

            Dir::Left => {
                let amt = ins.amt / 90;
                cidx -= amt;
                loop {
                    if cidx < 0 {
                        cidx += cardinal.len() as isize;
                        cidx %= cardinal.len() as isize;
                    } else {
                        break;
                    }
                }
            },

            _ => {}
        }
    }

    position.0.abs() + position.1.abs()
}

#[aoc(day12, part2)]
fn part2(input: &[Instruction]) -> isize {
    let mut waypoint = (10, 1);
    let mut ship = (0, 0);

    for ins in input {
        match ins.dir {
            Dir::North => waypoint.1 += ins.amt,
            Dir::South => waypoint.1 -= ins.amt,
            Dir::East => waypoint.0 += ins.amt,
            Dir::West => waypoint.0 -= ins.amt,

            Dir::Right => {
                let tmp_wpt = waypoint;
                match ins.amt {
                    90 => { waypoint.0 = tmp_wpt.1; waypoint.1 = -tmp_wpt.0 },
                    180 => { waypoint.0 = -tmp_wpt.0; waypoint.1 = -tmp_wpt.1 },
                    270 => { waypoint.0 = -tmp_wpt.1; waypoint.1 = tmp_wpt.0 },
                    _ => {},
                }
            },

            Dir::Left => {
                let tmp_wpt = waypoint;
                match ins.amt {
                    90 => { waypoint.0 = -tmp_wpt.1; waypoint.1 = tmp_wpt.0 },
                    180 => { waypoint.0 = -tmp_wpt.0; waypoint.1 = -tmp_wpt.1 },
                    270 => { waypoint.0 = tmp_wpt.1; waypoint.1 = -tmp_wpt.0 },
                    _ => {},
                }
            },

            Dir::Forward => {
                ship.0 += ins.amt * waypoint.0;
                ship.1 += ins.amt * waypoint.1;
            },
        }
    }

    ship.0.abs() + ship.1.abs()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn part_1() {
        let inp = r"F10
N3
F7
R90
F11";
        let inp = parse_input(inp);
        assert_eq!(part1(&inp), 25);
    }

    #[test]
    fn part_2() {
        let inp = r"F10
N3
F7
R90
F11";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 286);
    }
}
