use std::collections::{
    HashSet,
    HashMap,
};

use regex::Regex;

struct Bag {
    pub color: String,
    pub contains: Vec<(String, usize)>,
}

#[aoc_generator(day7)]
fn parse_input(input: &str) -> Vec<Bag> {
    let pat = Regex::new(r"^(\d+) (.+) bags?\.?").unwrap();
    input
        .lines()
        .map(|line| {
            let mut line = line.split("bags contain");
            let color = line.next().unwrap().trim();
            let line = line.next().unwrap().split(',');
            let contains = line.filter_map(|x| {
                if x.trim() == "no other bags." {
                    return None;
                }
                let caps = pat.captures(x.trim()).unwrap();
                let amt = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
                let color = caps.get(2).unwrap().as_str();

                Some((color.to_string(), amt))
            }).collect();

            Bag { color: color.to_string(), contains }
        }).collect()
}

#[aoc(day7, part1)]
fn part1(input: &[Bag]) -> usize {
    let mut bags = HashSet::new();
    let mut colors: Vec<&str> = Vec::new();
    colors.push("shiny gold");

    loop {
        let mut tmp_colors: Vec<&str> = Vec::new();

        for color in colors {
            for bag in input {
                if bag.contains.iter().any(|(bcolor, _amt)| bcolor == color) {
                    bags.insert(&bag.color);
                    tmp_colors.push(&bag.color);
                }
            }
        }

        if tmp_colors.is_empty() {
            break;
        }

        colors = tmp_colors;
    }

    bags.len()
}

fn get_all_bags(bag: &Bag, map: &HashMap<String, &Bag>) -> usize {
    if bag.contains.is_empty() {
        return 1;
    }

    bag.contains.iter().fold(1, |acc, (color, amt)| acc + amt * get_all_bags(map.get(color).unwrap(), map))
}

#[aoc(day7, part2)]
fn part2(input: &[Bag]) -> usize {
    let mut map: HashMap<String, &Bag> = HashMap::new();

    for bag in input {
        map.insert(bag.color.clone(), &bag);
    }

    let bag = map.get("shiny gold").unwrap();

    get_all_bags(&bag, &map) - 1 // don't count the shiny gold bag itself
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part2_ex1() {
        let inp = r"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 32);
    }

    #[test]
    fn test_part2_ex2() {
        let inp = r"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 126);
    }
}
