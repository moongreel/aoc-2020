use std::collections::{
    HashMap,
};

use regex::Regex;

#[aoc_generator(day4)]
fn parse_input(input: &str) -> Vec<HashMap<String, String>> {
    let mut passports: Vec<HashMap<String, String>> = Vec::new();
    let mut passport = HashMap::new();
    for line in input.lines() {
        if line.is_empty() {
            passports.push(passport);
            passport = HashMap::new();
            continue;
        }

        line.split(' ').for_each(|x| {
            if x.contains(':') {
                let mut x = x.split(':');
                let key = x.next().unwrap().to_string();
                let val = x.next().unwrap().to_string();
                passport.insert(key, val);
            }
        });
    }
    passports.push(passport);

    passports
}

fn valid_passport(passport: &HashMap<String, String>) -> bool {
    let mut valid = true;
    let fields = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    for field in fields.iter() {
        if !passport.contains_key(&field.to_string()) {
            valid = false;
        }
    }
    valid
}

#[aoc(day4, part1)]
fn part1(input: &[HashMap<String, String>]) -> usize {
    let mut valid_passes = 0;
    let fields = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    for passport in input {
        let mut valid = true;
        for field in fields.iter() {
            if !passport.contains_key(&field.to_string()) {
                valid = false;
            }
        }
        if valid {
            valid_passes += 1;
        }
    }

    valid_passes
}

#[aoc(day4, part2)]
fn part2(input: &[HashMap<String, String>]) -> usize {
    let mut valid_passes = 0;
    let passports: Vec<_> = input
        .iter()
        .filter(|x| valid_passport(x))
        .collect();

    for passport in passports {
        let mut valid = true;
        for (key, value) in passport.iter() {
            match key.as_ref() {
                "byr" => {
                    let pat = Regex::new(r"^\d{4}$").unwrap();
                    if !pat.is_match(value) {
                        valid = false;
                        break;
                    }
                    if let Ok(val) = value.parse::<usize>() {
                        valid = val >= 1920 && val <= 2002;
                    } else { valid = false; }
                },
                "iyr" => {
                    let pat = Regex::new(r"^\d{4}$").unwrap();
                    if !pat.is_match(value) {
                        valid = false;
                        break;
                    }
                    if let Ok(val) = value.parse::<usize>() {
                        valid = val >= 2010 && val <= 2020;
                    } else { valid = false; }
                },
                "eyr" => {
                    let pat = Regex::new(r"^\d{4}$").unwrap();
                    if !pat.is_match(value) {
                        valid = false;
                        break;
                    }
                    if let Ok(val) = value.parse::<usize>() {
                        valid = val >= 2020 && val <= 2030;
                    } else { valid = false; }
                },
                "hgt" => {
                    let pat = Regex::new(r"(\d+)(cm|in)").unwrap();
                    if !pat.is_match(value) {
                        valid = false;
                    } else {
                        let caps = pat.captures(value).unwrap();
                        let num = caps.get(1).unwrap().as_str();
                        let t = caps.get(2).unwrap().as_str();

                        if let Ok(num) = num.parse::<usize>() {
                            match t {
                                "cm" => {
                                    valid = num >= 150 && num <= 193;
                                },
                                "in" => {
                                    valid = num >= 59 && num <= 76;
                                }
                                _ => valid = false,
                            }
                        }
                    }
                },
                "hcl" => {
                    let pat = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
                    valid = pat.is_match(value);
                },
                "ecl" => {
                    let pat = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
                    valid = pat.is_match(value);
                },
                "pid" => {
                    let pat = Regex::new(r"^\d{9}$").unwrap();
                    valid = pat.is_match(value);
                },
                "cid" => {},
                _ => valid = false,
            }
            if !valid { break; }
        }
        if valid {
            valid_passes += 1;
        }
    }

    valid_passes
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part2_valid() {
        let inp = r"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 4);
    }

    #[test]
    fn test_part2_invalid() {
        let inp = r"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp), 0);
    }
}
