use std::collections::{
    HashSet,
};

use regex::Regex;

#[derive(Clone)]
struct Instruction {
    pub operand: isize,
    pub opcode: String,
}

#[aoc_generator(day8)]
fn parse_input(input: &str) -> Vec<Instruction> {
    let pat = Regex::new(r"^(\w+) ([\+\-]\d+)$").unwrap();
    input
        .lines()
        .map(|x| {
            let caps = pat.captures(x).unwrap();
            let opcode = caps.get(1).unwrap().as_str().to_string();
            let operand = caps.get(2).unwrap().as_str().parse::<isize>().unwrap();
            Instruction { operand, opcode }
        })
        .collect()
}

#[aoc(day8, part1)]
fn part1(input: &[Instruction]) -> isize {
    let mut pc: isize = 0;
    let mut acc = 0;
    let mut set = HashSet::new();

    loop {
        if set.contains(&pc) {
            break;
        }

        set.insert(pc);

        let ins = input.get(pc as usize).unwrap();
        match ins.opcode.as_ref() {
            "acc" => acc += ins.operand,
            "jmp" => pc += ins.operand - 1,
            _ => {}
        }
        pc += 1;
    }

    acc
}

#[aoc(day8, part2)]
fn part2(input: &[Instruction]) -> isize {
    let mut idx = 0;

    loop {
        let mut inp = input.to_vec();
        loop {
            if idx >= input.len() {
                panic!("Checked all instructions");
            }
            if inp[idx].opcode == "nop" {
                inp[idx].opcode = "jmp".to_string();
                break;
            } else if inp[idx].opcode == "jmp" {
                inp[idx].opcode = "nop".to_string();
                break;
            } else {
                idx += 1;
            }
        }

        let mut pc: isize = 0;
        let mut acc = 0;
        let mut set = HashSet::new();
        loop {
            if pc >= inp.len() as isize {
                return acc;
            }

            if set.contains(&pc) {
                break;
            }

            set.insert(pc);

            let ins = inp.get(pc as usize).unwrap();
            match ins.opcode.as_ref() {
                "acc" => acc += ins.operand,
                "jmp" => pc += ins.operand - 1,
                _ => {}
            }
            pc += 1;
        }
        idx += 1;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part2() {
        let inp = r"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let inp = parse_input(inp);
        assert_eq!(part2(&inp),8);
    }
}
