use std::collections::HashMap;

#[aoc_generator(day3)]
fn parse_input(input: &str) -> (HashMap<(usize, usize), bool>, usize, usize) {
    let mut map = HashMap::new();

    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            match c {
                '.' => map.insert((x, y), false),
                '#' => map.insert((x, y), true),
                _ => panic!("bad input"),
            };
        }
    }

    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();

    (map, width, height)
}

#[aoc(day3, part1)]
fn part1(input: &(HashMap<(usize, usize), bool>, usize, usize)) -> usize {
    let (map, width, height) = input;

    let mut x = 0;
    let mut trees = 0;

    for y in 1..*height {
        let pt = ( (x+3) % *width, y);

        if *map.get(&pt).unwrap() {
            trees += 1;
        }
        
        x += 3;
    }

    trees
}

#[aoc(day3, part2)]
fn part2(input: &(HashMap<(usize, usize), bool>, usize, usize)) -> usize {
    let (map, width, height) = input;

    let mut tot = 1;

    let slopes = vec![(1,1), (3,1), (5,1), (7,1), (1,2)];

    for (dx, dy) in slopes {
        let mut trees = 0;
        for y in 1..(*height/dy) {
            if y + dy > *height {
                break;
            }

            let pt = ( (y*dx) % *width, y*dy);

            if *map.get(&pt).unwrap() {
                trees += 1;
            }
        }
        tot *= trees;
    }

    tot
}
