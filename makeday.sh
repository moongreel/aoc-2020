#!/bin/bash
last_day="$(ls src/day*rs | grep -o '[0-9]\+' | sort -h | tail -1)"
next_day="$((${last_day} + 1))"


cp src/template.rs src/day${next_day}.rs
sed -si "s/<DAY>/${next_day}/g" src/day${next_day}.rs
sed -si "s/pub mod day${last_day}\;/\0\npub mod day${next_day}\;/g" src/lib.rs
